function generateArray() {
    let arrLen = Math.floor((Math.random() * 100) + 1);
    let arr = []
    const maxRange = 100;

    for(let i = 0; i < arrLen; i++) {
        arr.push(Math.floor((Math.random() * maxRange) + 1));
    }

    return arr;
}// end generateArray


function binarySearch(arr, value) {
    let low = 0;
    let high = arr.length - 1;

    while(low <= high) {
        let mid = Math.floor((low + high) / 2);

        if(arr[mid] > value) {
            high = mid - 1;
        } else if(arr[mid] < value) {
            low = mid + 1;
        } else {
            return mid;
        }
    }

    return -1;
}// end binarySearch

var a = generateArray();
a.sort(function (a, b) {
    return a - b;
});

console.log('a,', a);
var result = binarySearch([1,2,3,4,5], 3);

console.log(result);
